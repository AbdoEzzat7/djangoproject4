from rest_framework import  generics
from articles.models import Article
from users.models import CustomUser
from .serializers import ArticleSerializer
from .serializers import CustomUserSerializer

# Create your views here.
class ArticleApiView(generics.ListAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
class ArticleApiDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset =Article.objects.all()
    serializer_class = ArticleSerializer
class UsersApiView(generics.ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
class UsersApiDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
