from django.urls import path
from .views import ArticleApiView, ArticleApiDetail,UsersApiView,UsersApiDetail

urlpatterns = [
    path("<int:pk>/", ArticleApiDetail.as_view()),
    path("",ArticleApiView.as_view()),
    path("users/",UsersApiView.as_view()),
    path("users/<int:pk>/",UsersApiDetail.as_view()),
]