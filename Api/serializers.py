from rest_framework import serializers
from articles.models import Article
from users.models import CustomUser
class ArticleSerializer(serializers.ModelSerializer):
    class Meta :
        model = Article
        fields = ("title","body","date","author")
class CustomUserSerializer(serializers.ModelSerializer):
    class Meta :
        model = CustomUser
        fields = (
            "username",
            "email",
            "age",
        )

